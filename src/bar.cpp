#include "bar.hpp"

Bar* Bar::inst = nullptr;

Bar::Bar()
{
}

Bar::~Bar()
{
    delete[] bufferPedidos;
    delete[] bufferEntregas;
    delete[] printRodada;
    delete[] clientesAtendidos;
    delete[] drinkPronto;
    delete inst;

    pthread_mutex_destroy(&mutexGarcom);
    pthread_mutex_destroy(&mutexGarcomEntrega);
    pthread_mutex_destroy(&mutexPrint);
    pthread_mutex_destroy(&mutexRodada);
    pthread_cond_destroy(&idBalcaoGarcom);
    pthread_cond_destroy(&novaRodada);
    pthread_cond_destroy(drinkPronto);
    pthread_cond_destroy(&idBalcaoCliente);
    pthread_cond_destroy(&entregaProximo);

    for(int i = 0; i < qtdGarcons; i++)
    {
        for(int j = 0; j < capacidadePedidos; j++)
        {
            delete[] matrixGarcomPedidoCliente[i][j];
        }
        delete[] matrixGarcomPedidoCliente[i];
    }
    delete[] matrixGarcomPedidoCliente;
}

Bar* Bar::instanciaSingleton()
{
    if(inst == nullptr)
    {
        inst = new Bar();
    }
    return inst;
}

void Bar::abreBar(int nClientes, int nGarcons, int capacidade, int nRodadas)
{
    rodadaMax = nRodadas;
    qtdClientes = nClientes;
    qtdGarcons = nGarcons;
    capacidadePedidos = capacidade;

    pthread_mutex_init(&mutexGarcom, 0);
    pthread_mutex_init(&mutexGarcomEntrega, 0);
    pthread_mutex_init(&mutexPrint, 0);
    pthread_mutex_init(&mutexRodada, 0);

    drinkPronto = new pthread_cond_t[qtdClientes];

    pthread_cond_init(&idBalcaoGarcom, 0);
    pthread_cond_init(&novaRodada, 0);
    pthread_cond_init(drinkPronto, 0);
    pthread_cond_init(&idBalcaoCliente, 0);
    pthread_cond_init(&entregaProximo, 0);

    

    matrixGarcomPedidoCliente = new int**[qtdGarcons];
    for(int i = 0; i < qtdGarcons; i++)
    {
        matrixGarcomPedidoCliente[i] = new int*[capacidadePedidos];
        for(int j = 0; j < capacidadePedidos; j++)
        {
            matrixGarcomPedidoCliente[i][j] = new int[2];
        }
    }

    for(int i = 0; i < qtdGarcons; i++)
    {
        for(int j = 0; j < capacidadePedidos; j++)
        {
            matrixGarcomPedidoCliente[i][j][0] = -1;
            matrixGarcomPedidoCliente[i][j][1] = -1;
        }
    }

    printRodada = new bool[rodadaMax];
    clientesAtendidos = new int[rodadaMax];

    for(int i = 0; i < rodadaMax; i++)
    {
        clientesAtendidos[i] = 0;
        printRodada[i] = true;
    }

    bufferPedidos = new int[qtdGarcons];
    bufferEntregas = new int[qtdGarcons];

    for(int i = 0; i < qtdGarcons; i++)
    {
        bufferPedidos[i] = 0;
        bufferEntregas[i] = 0;
    }

    abertoBar = true;
}

void Bar::fechaBar()
{
    abertoBar = false;
}

bool Bar::getBarStatus()
{
    return abertoBar;
}

int Bar::getRodadaMax(){
    return rodadaMax;
}

int Bar::getNumeroClientes()
{
    return qtdClientes;
}

int Bar::getClientesAtendidos(int rodada)
{
    if(rodada == -5) return atendidos;
    return clientesAtendidos[rodada];
}

void Bar::passaRodada()
{
    terminaRodada = true;
    clientesAtendidos[rodadaAtual] = 0;
    rodadaAtual++;
    printMensagem("PASSA");
    contCliente = 0;
    acordaGeral();
    if(rodadaAtual >= rodadaMax) 
    {
        fechaBar();
    }
}

void Bar::printMensagem(const char* mensagem)
{
    pthread_mutex_lock(&mutexPrint);
    std::cout << mensagem << std::endl;
    pthread_mutex_unlock(&mutexPrint);
}

void Bar::printMensagem(int idGarcom, int idCliente, int versao, int arg4)
{
    pthread_mutex_lock(&mutexPrint);
    switch (versao)
    {
        case 0:
            std::cout << "CLIENTE " << idCliente << " \033[0;36mpediu\033[0m " << cardapio[arg4] << std::endl;
            break;
        case 1:
            std::cout << "GARCOM " << idGarcom << " \033[0;31manotou\033[0m o pedido do CLIENTE " << idCliente << std::endl;
            break;
        case 2:
            std::cout << "GARCOM " << idGarcom << " \033[0;32mentregou\033[0m " << cardapio[arg4] << " para CLIENTE " << idCliente << std::endl;
            break;
        case 3:
            std::cout << "CLIENTE " << idCliente << " \033[0;33mespera\033[0m sua bebida" << std::endl;
            break;
        case 4:
            std::cout << "CLIENTE " << idCliente << " \033[0;34mcomeca\033[0m a tomar sua bebida" << std::endl;
            break;
        case 5:
            std::cout << "CLIENTE " << idCliente << " \033[0;35mtoma\033[0m sua bebida em " << arg4 << " ciclos" << std::endl;
            break;
        case 6:
            std::cout << "ARG: " << arg4 << std::endl;
            break;
        case 7:
            std::cout << "=-=-=-=-=-=-=-=-=-=-=-=------BAR  FECHADO------=-=-=-=-=-=-=-=-=-=-=-=" << std::endl;
            break;
    }
pthread_mutex_unlock(&mutexPrint);
}

int Bar::getBufferPedidos(int idGarcom)
{
    return bufferPedidos[idGarcom];
}

void Bar::esperaRodada()
{
    if((rodadaAtual+1) >= rodadaMax) return;
    pthread_mutex_lock(&mutexRodada);
    pthread_cond_wait(&novaRodada, &mutexRodada);
    pthread_mutex_unlock(&mutexRodada);
    contCliente = 0;
}

int Bar::getRodadaAtual()
{
    return rodadaAtual;
}
void Bar::acordaGeral()
{
    pthread_cond_broadcast(&novaRodada);
}
void Bar::acordaGarcom()
{
    pthread_cond_broadcast(&idBalcaoGarcom);
}

bool Bar::getPrintRodada(int rodada)
{
    return printRodada[rodada];
}

void Bar::printaRodada(int rodada)
{
    pthread_mutex_lock(&mutexPrint);
    if(printRodada[rodada] == true)
    {
        std::cout << "=-=-=-=-=-=-=-=-=-=-=-=---INICIO DA RODADA " << rodada+1 << "---=-=-=-=-=-=-=-=-=-=-=-=" << std::endl;
        printRodada[rodada] = false;
    }
    pthread_mutex_unlock(&mutexPrint);
}

int Bar::getClientesSairam()
{
    return clientesSairam;
}

void Bar::incClientesSairam()
{
    clientesSairam++;
}

int Bar::getAtendidos()
{
    return atendidos;
}

//-----------------------FUNCOES REALIZADAS PELO GARCOM

//----------DENTRO DE recebeMaximoPedidos()

int Bar::getCapacidade()
{
    return capacidadePedidos;
}

int Bar::waitRealizarPedido(int idGarcom, int idPedido)
{
    pthread_mutex_lock(&mutexGarcom);
    while((clientesAtendidos[rodadaAtual] < qtdClientes) && buffer == 0)
    {
        if(clientesAtendidos[rodadaAtual] == 0 || indexBebida == 0) pthread_cond_signal(&idBalcaoCliente);
        if(abertoBar != false) pthread_cond_wait(&idBalcaoGarcom, &mutexGarcom);
        if(abertoBar == false || terminaRodada == true)
        {
            contadorGarcons++;
            if(contadorGarcons == (qtdGarcons-1))
            {
                contadorGarcons = 0;
                terminaRodada = false;
            }
            pthread_mutex_unlock(&mutexGarcom);
            return 2;
        }
    }
    if(abertoBar == false)
    {
        pthread_mutex_unlock(&mutexGarcom);
        return 2;
    }
    buffer = 0;
    if(clientesAtendidos[rodadaAtual] == qtdClientes)
    {
        passaRodada();
        pthread_mutex_unlock(&mutexGarcom);
        return 2;
    }
    matrixGarcomPedidoCliente[idGarcom][idPedido][0] = indexCliente;
    matrixGarcomPedidoCliente[idGarcom][idPedido][1] = indexBebida;
    bufferPedidos[idGarcom]++;
    printMensagem(idGarcom, indexCliente, 1, -1);
    atendidos++;
    clientesAtendidos[rodadaAtual]++;
    pthread_cond_signal(&idBalcaoCliente);
    pthread_mutex_unlock(&mutexGarcom);
    return 0;
}

//-------------DENTRO DE entregaPedidos

void Bar::drinkClientePronto(int idGarcom, int bufferBusca)
{
    pthread_mutex_lock(&mutexGarcomEntrega);
    bufferBusca = bufferBusca;
    bool entregaLiberada = false;
    int i = 0;
    for(i = 0; i < capacidadePedidos; i++)
    {
        pthread_mutex_lock(&mutexPrint);
        pthread_mutex_unlock(&mutexPrint);
        if(matrixGarcomPedidoCliente[idGarcom][i][0] == queueEntrega.front())
        {
            entregaLiberada = true;
            break;
        }
    }
    if(entregaLiberada == true)
    {
        printMensagem(idGarcom, queueEntrega.front(), 2, matrixGarcomPedidoCliente[idGarcom][i][1]);
        pthread_cond_signal(&(drinkPronto[queueEntrega.front()]));
        queueEntrega.pop();
        if( queueEntrega.empty() == 0)
        {
            atendidos = 0;
        }
        bufferPedidos[idGarcom]--;
        pthread_cond_broadcast(&entregaProximo);
    } else {
        pthread_cond_wait(&entregaProximo, &mutexGarcomEntrega);
    }
    pthread_mutex_unlock(&mutexGarcomEntrega);
}

void Bar::resetBufferPedidos(int idGarcom)
{
    bufferPedidos[idGarcom] = 0;
    for(int j = 0; j < capacidadePedidos; j++)
    {
        matrixGarcomPedidoCliente[idGarcom][j][0] = -1;
        matrixGarcomPedidoCliente[idGarcom][j][1] = -1;
    }
}

//-----------------------FUNCOES REALIZADAS PELO CLIENTE

//--------dentro do  fazPedido

int Bar::waitGarcons(int idCliente, int idBebida){
    pthread_mutex_lock(&mutexGarcom);
    int retorno = 0;
    while(buffer == 1)
    {
        pthread_cond_wait(&idBalcaoCliente, &mutexGarcom);
    }
    buffer = 1;
    indexCliente = idCliente;
    indexBebida = idBebida;
    contCliente++;
    printMensagem(-1, idCliente, 0, idBebida);
    if(indexBebida == 0)
    {
        if(contCliente == qtdClientes)
        {
            retorno = 1;
        }else{
            retorno = 5;
            buffer = 0;
        }
        clientesAtendidos[rodadaAtual] = contCliente;
        pthread_cond_signal(&idBalcaoGarcom);
        pthread_mutex_unlock(&mutexGarcom);
    } else {
        queueEntrega.push(idCliente);
    }
    return retorno;
}

//--------dentro do esperaPedido

//--------dentro do recebePedido
void Bar::pegaPedido(int idCliente)
{
    pthread_mutex_lock(&mutexGarcomEntrega);
    printMensagem(-1, idCliente, 3, -1);
    pthread_cond_signal(&idBalcaoGarcom);
    pthread_mutex_unlock(&mutexGarcom);
    pthread_cond_wait(&(drinkPronto[idCliente]), &mutexGarcomEntrega);
    pthread_cond_broadcast(&entregaProximo);
    pthread_mutex_unlock(&mutexGarcomEntrega);
}

//-------DENTRO DE consomePedido
//-------------------------------------------