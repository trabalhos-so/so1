#include <iostream>
#include <queue>
#include "bar.hpp"
#include "cliente.hpp"
#include "garcom.hpp"

using namespace std;

void* routineCliente(void* arg)
{
    Bar* ptrBar = Bar::instanciaSingleton();
    int rodada = 0;
    int rodadaMax = ptrBar->getRodadaMax();
    while(rodada < rodadaMax)
    {
        ptrBar->printaRodada(rodada);
        int returnFazPedido = ((Cliente*)arg)->fazPedido();
        if( returnFazPedido == 0)
        {
            ((Cliente*)arg)->esperaPedido();
            ((Cliente*)arg)->recebePedido();
            ((Cliente*)arg)->consomePedido();
        }else if(returnFazPedido == 5){
            ptrBar->esperaRodada();
        }
        rodada++;
        if(rodada == rodadaMax) break;
    }
    ptrBar->incClientesSairam();
    if(ptrBar->getClientesSairam() == ptrBar->getNumeroClientes()) ptrBar->fechaBar();
    ptrBar->acordaGarcom();
    pthread_exit(nullptr);
}

/*
Rotina do garcom:
-quando comeca avisa o cliente que esta livre
-aumenta o buffer de garcons livres
-realiza o restantes de suas funcoes
*/

void* routineGarcom(void* arg)
{
    Bar* ptrBar = Bar::instanciaSingleton();
    while((ptrBar->getBarStatus() != false))
    {
        if( ((Garcom*)arg)->recebeMaximoPedidos() == 0)
        {
            ((Garcom*)arg)->entregaPedidos();
        }
    }
    ptrBar->acordaGeral();
    pthread_exit(nullptr);
}

int main(int argc, char* argv[])
{
    if(argc != 5)
    {
        cout << "Parâmetros inválidos!" << endl;
        cout << "Primeiro parâmetro: Numero de clientes presentes no estabelecimento." << endl;
        cout << "Segundo parâmetro: Numero de garcons que estao trabalhando." << endl;
        cout << "Terceiro parâmetro: Capacidade de atendimento dos garcons." << endl;
        cout << "Quarto parâmetro: Numero de rodadas que serao liberadas no bar." << endl;
        return 1;
    }

    int numeroClientes = stoi(argv[1],nullptr);
    int numeroGarcons = stoi(argv[2],nullptr);
    int capacidadeAtendimento = stoi(argv[3],nullptr);
    int numeroRodadas = stoi(argv[4],nullptr);

    Bar* ptrBar = Bar::instanciaSingleton();
    ptrBar->abreBar(numeroClientes, numeroGarcons, capacidadeAtendimento, numeroRodadas);

    pthread_t garcons[numeroGarcons];
    pthread_t cliente[numeroClientes];
    Garcom* PonteiroGarcom;
    Cliente* PonteiroCliente;
    queue<Garcom*> queueGarcom;
    queue<Cliente*> queueCliente;

    for(int id = 0; id < numeroGarcons; id++)
    {
        PonteiroGarcom = new Garcom(id);
        queueGarcom.push(PonteiroGarcom);
        pthread_create(&garcons[id], nullptr, routineGarcom, (void*)PonteiroGarcom);
    }
    for(int id = 0; id < numeroClientes; id++)
    {
        PonteiroCliente = new Cliente(id);
        queueCliente.push(PonteiroCliente);
        pthread_create(&cliente[id], nullptr, routineCliente, (void*)PonteiroCliente);
    }
    
    for(int id = 0; id < numeroClientes; id++)
    {
        pthread_join(cliente[id], nullptr);
    }

    for(int id = 0; id < numeroGarcons; id++)
    {
        pthread_join(garcons[id], nullptr);
    }

    ptrBar->printMensagem(-1, -1, 7, -1);

    while(!queueCliente.empty())
    {
        Cliente* ptrDel = queueCliente.front();
        queueCliente.pop();
        delete ptrDel;
    }
    while(!queueGarcom.empty())
    {
        Garcom* ptrDel = queueGarcom.front();
        queueGarcom.pop();
        delete ptrDel;
    }

    return 0;
}