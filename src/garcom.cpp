#include "garcom.hpp"

Garcom::Garcom(int idGarcom)
{
    identificacao = idGarcom;
}
Garcom::~Garcom()
{}
/*
Enquanto garcom nao recebeu seus C pedidos:
----ele espera os sinais do cliente confirmando o pedido
-depois que relizou os C pedidos ele diminui um no buffer de 
-garcons disponiveis e passa a vez para outro garcon
*/
int Garcom::recebeMaximoPedidos()
{
    Bar* ptrBar = Bar::instanciaSingleton();
    int contadorPedidos = 0;
    int capacidadeGarcom = ptrBar->getCapacidade();
    rodadaGarcom = ptrBar->getRodadaAtual();
    while((ptrBar->getBufferPedidos(identificacao) < capacidadeGarcom) && (rodadaGarcom == ptrBar->getRodadaAtual()) )
    {
        if (ptrBar->waitRealizarPedido(identificacao, contadorPedidos) == 2) break;
        contadorPedidos++;
    }
    estadoGarcom = PREPARANDO;
    if(ptrBar->getBufferPedidos(identificacao) == 0)
    {
        estadoGarcom = ATENDENDO;
        return 1;
    }
    return 0;
}

/*
Garcom chega com os C drinks que foram pedidos
-enquanto ele nao entrega todos os C drinks:
---ele coloca um drink no balcao e espera o sinal de que o cliente retirou 
---diminui um dos drinks do buffer do total de drinks
---entao coloca mas um drink na mesa e manda sinal pro cliente 
-quando o garcom entregar todos passa a vez para outro garcom
*/

void Garcom::entregaPedidos()
{
    Bar* ptrBar = Bar::instanciaSingleton();
    while(ptrBar->getBufferPedidos(identificacao) > 0)
    {

        ptrBar->drinkClientePronto(identificacao, ptrBar->getBufferPedidos(identificacao));
    }
    ptrBar->resetBufferPedidos(identificacao);
    estadoGarcom = ATENDENDO;
}