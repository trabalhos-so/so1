#include "cliente.hpp"

Cliente::Cliente(int idCliente)
{
    identificacao = idCliente;
}

Cliente::~Cliente()
{}

int Cliente::aleatorio(int inicio, int fim){
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(inicio, fim);
    return dis(gen);
}

/*
enquanto nao houver garcons disponiveis espera o sinal de garcons_livres
quando for liberado garcom aumenta buffer dos pedidos do garcom e envia
um sinal para o garcom para que este confira se seu limite ja foi alcancado.
Apos isso da a vez para outro cliente pedir
*/

int Cliente::fazPedido()
{
    Bar* ptrBar = Bar::instanciaSingleton();
    bebidas = aleatorio(0,8);
    int returnWaitGarcons = ptrBar->waitGarcons(identificacao, bebidas);
    rodadaCliente++;
    return returnWaitGarcons;
}


void Cliente::esperaPedido()
{
    if(estadoCliente == PEDIU)
    {
        estadoCliente = ESPERANDO;
    }
}
/*
Cliente vai  receber o pedido
-enquanto nao tem drink na balcao:
---espera o sinal do garcom de que o drink foi colocado
-apos pegar reduz o buffer de drink no balcao
-manda o sinal para o garcon de que retirou
-passa a vez para outro cliente retirar
*/
void Cliente::recebePedido()
{
    Bar* ptrBar = Bar::instanciaSingleton();
    ptrBar->pegaPedido(identificacao);
    estadoCliente = CURTINDO;
}

/*
fica em espera por um tempo aleatorio
entre 500 e 1000 ciclos
*/
void Cliente::consomePedido()
{
    Bar* ptrBar = Bar::instanciaSingleton();
    int tempoConsumo = aleatorio(500, 1000);
    for(int i = 0; i < tempoConsumo; i++){}
    ptrBar->printMensagem(-1, identificacao, 5, tempoConsumo);
    if(rodadaCliente != ptrBar->getRodadaAtual())
    {
        if(ptrBar->getRodadaAtual() == ptrBar->getRodadaMax()) return;
        ptrBar->esperaRodada();
    }
}