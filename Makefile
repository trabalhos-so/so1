SRC=$(shell find ./src/ -name "*.cpp" -type f)
$(shell mkdir -p objects)
OBJ=$(patsubst ./src%, ./objects%, $(patsubst %.cpp, %.o , $(SRC)))
FLAGS=-Wall -g -pthread -Wextra -Wshadow -Werror -I include

all: exeBar

exeBar: $(OBJ)
	g++ -o $@ $^ $(FLAGS)

objects/%.o: src/%.cpp
	g++ -c -o $@ $^ $(FLAGS)

run: exeBar
	./exeBar

clean:
	@find . -type f -name "*.o" -exec rm '{}' \;
	@find . -type f -executable -exec rm '{}' \;
remake:
	$(MAKE) clean
	$(MAKE)