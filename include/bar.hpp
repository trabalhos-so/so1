#ifndef BAR_H_
#define BAR_H_

#define PEDIU 0
#define ESPERANDO 1
#define CURTINDO 2
#define ATENDENDO 0
#define PREPARANDO 1
#define ENTREGANDO 2

#include <pthread.h>
#include <iostream>
#include <queue>

const char cardapio[][15] = {"nada","vodka", "corote", "chopp", "cerveja", "agua", "refrigerante", "tonica", "tequila"};

class Bar {
    public:
        /*============== DESTRUTOR ===============
            Destrutor padrao
          ==================================================*/
        ~Bar();

        /*============== ABREBAR ===============
            Inicializa variaveis
          ==================================================*/
        void abreBar(int,int,int,int);

        /*============== FECHABAR ===============
            Controla a variavel abertoBar
          ==================================================*/
        void fechaBar();

        /*============== GETBARSTATUS ===============
            Retorna a variavel abertoBar
          ==================================================*/

        bool getBarStatus();

        /*============== GETNUMEROCLIENTES ===============   
            Retorna a variavel qtdClientes
          ==================================================*/

        int getNumeroClientes();

            /*============== SINGLETON ===============   
            Metodos que tornam esta classe uma Singleton
          ==================================================*/

        Bar(Bar &other) = delete;
        void operator=(const Bar &) = delete;
        static Bar* instanciaSingleton();

        /*============== METODO PASSA RODADA ===============
            Chamado para passar as rodadas do bar. Esse meto-
        do zera os clientes atendidos, incrementa o atributo
        rodadaAtual e mais importante acorda os garcons e cli-
        entes que estao em espera. Por fim se for a ultima ro-
        dada, essa funcao fecha o bar.
          ==================================================*/

        void passaRodada();

         /*============== METODO WAIT GARCONS ===============
            Metodo chamado para que o cliente possa realizar
        pedidos. Entra com o numero de identificacao do clien-
        te e o numero da bebida que o cliente deseja. Com o 
        buffer interage com o garcom para que nao haja perca 
        de informacaos. Sendo uma regiao critica requer o 
        uso de mutexes para sincronizacao com o garcom. O cli-
        ente pode nao querer nada. Incrementa o atributo
        clientesAtendidos[rodadaAtual] caso sua escolha seja 
        nao beber nada e entao manda um sinal para liberar o
        garcom. Caso o cliente queira beber algo o garcon re-
        cebe seu pedido e o ID do cliente entra na fila.
          ==================================================*/

        int waitGarcons(int,int);

        /*============== METODO GET CAPACIDADE ===============
            Metodo que retorna a capacidade individual de aten-
        dimento de cada garcom.
          ==================================================*/

        int getCapacidade();

        /*============== METODO GET BUFFER PEDIDOS===============
            Retorna o buffer individual de pedidos de cada garcom
        ao ser passado o ID do garcom.
          ==================================================*/

        int getBufferPedidos(int);

        /*============== METODO WAIT REALIZA PEDIDOS ===============
            Regiao critica protagonizada pelo garcom. Nela o garcom
        troca informacoes com os cliente retirando os pedidos e suas
        identificacoes. O garcom atua enquanto tem clientes a serem 
        atendidos na rodada e tem sua liberacao para retirada do pe-
        dido atraves do buffer e do sinal que o cliente envia. O 
        garcom confere se todos os clientes foram atendidos e entao 
        passa a rodada se for o caso. Apos isso recebe as informa-
        coes do cliente em sua matriz e incrementa os clientes que
        foram atendidos.
          ==================================================*/

        int waitRealizarPedido(int, int);

        /*============== METODO DRINK CLIENTE PRONTO ===============
            Regiao critica de entrega dos drinks. O garcom enquanto
        tem pedidos a ser entregues, testa se possui o primeiro 
        cliente na ordem de entregas. O teste se realiza varrendo a
        matriz do garcom e procurando o ID do cliente a cada vez que
        um sinal de entrega broadcast for enviado. Se o garcom possuir
        o primeiro ID, a entrega vai ser liberada e esse primeiro
        cliente sai da fila. Se nao for liberado volta a esperar sinal
        para nova busca.
          ==================================================*/

        void drinkClientePronto(int, int);

        /*============== METODO PEGA PEDIDO===============
            Regiao critica de retirada dos pedidos com o cliente 
        atuando. Somente quando o cliente entra nessa regiao que 
        ele envia sinal para o garcom que registrou seu pedido, 
        com o intuito de uma melhor sincronizacao. Logo ao chegar
        fica esperando por sua vez na fila para receber o 
        seu drink. E ao receber envia sinal broadcast para que os
        garcons entreguem o proximo drink.
          ==================================================*/

        void pegaPedido(int);
        void waitClienteRetirar(int);
        void confirmaPedido(int);
        void waitEsperaPedido(int);
        void incrementoAtendidos();
        int getClientesAtendidos(int); 

        /*============== METODO ESPERA RODADA ===============
            Metodo que os clientes sao enviados caso terminaram
        suas bebidas antes da virada da rodada ou caso nao tenha 
        bebido nada na rodada. A entrada denomina se o cliente
        veio apos tomar bebida ou nao. Se estiver na ultima rod-
        da nao ha necessidade de espera. Quando o ultimo cliente
        entrar na espera, um sinal vai ser enviado para que os 
        garcons confirmem a virada da rodada. Os clientes sao 
        liberados na virada da rodada por um broadcast.
          ==================================================*/

        void esperaRodada();

        /*============== METODO GET RODADA ATUAL ===============
            Retorna o numero da rodada atual
          ==================================================*/

        int getRodadaAtual();

        /*============== METODO ACORDA GERAL ===============
            Metodo acorda geral vai ser utilizado na virada
        da rodada enviando um sinal de broadcast para todos
        os clientes que estao esperando.
          ==================================================*/

        void acordaGeral();

        /*============== METODO RESET BUFFER PEDIDOS ===============
            Metodo importante para resetar o buffer de pedidos de cada
        garcom quando este termina de fazer todas as suas entregas.
        Sua matriz vai ser setada para -1.
          ==================================================*/

        void resetBufferPedidos(int);



        /*============== METODO GET RODADA MAX ===============
            Metodo que retorna numero maximo de rodadas.
          ==================================================*/

        int getRodadaMax();

        /*============== METODO GET PRINT RODADA===============
            Chama o metodo printaRodada enviando o numero da 
        rodada atual.
          ==================================================*/

        bool getPrintRodada(int);

         /*============== METODO PRINTA RODADA===============
            Metodo que faz o print das rodadas quando o progra-
        ma for executado para melhor entendimento do funciona-
        mento do bar.
          ==================================================*/

        void printaRodada(int);

         /*============== METODO ACORDA GARCOM===============
            Metodo que envia um sinal de broadcast para todos 
        os garcons que estao dormindo em determinado momento
        no bar.
          ==================================================*/

        void acordaGarcom();
        void acordaNovaRodada();
        int getAtendidos();

         /*============== METODO CLIENTES SAIRAM===============
            Metodo que retorna a quantidade de clientes que 
        sairam do bar.
          ==================================================*/

        int getClientesSairam();

         /*============== METODO INC CLIENTES SAIRAM===============
            Metodo que faz a contagem dos clientes que sairam 
        do bar.
          ==================================================*/

        void incClientesSairam();

         /*============== METODO PRINT MENSAGEM===============
            Metodo que printa informacoes importantes do bar
          ==================================================*/

        void printMensagem(const char*);

         /*============== METODO PRINT MENSAGEM===============
            Este metodo diferente do outro printa apenas infor-
        macoes dos acontecimentos do bar.
          ==================================================*/
        
        void printMensagem(int, int, int, int);
    private:

        int* clientesAtendidos;
        int* bufferPedidos;
        bool* printRodada;
        int* bufferEntregas;
        
        int rodadaMax;
        int qtdClientes;
        int qtdGarcons;
        int capacidadePedidos;
        bool abertoBar = false;
        int rodadaAtual = 0;
        bool terminaRodada = false;
        int contadorGarcons = 0;
        int buffer = 0;
        int contCliente = 0;
        int atendidos = 0;
        int clientesSairam = 0;

        std::queue<int> queueEntrega;

        pthread_mutex_t mutexGarcom;
        pthread_mutex_t mutexGarcomEntrega;
        pthread_mutex_t mutexPrint;
        pthread_mutex_t mutexRodada;

        pthread_cond_t* drinkPronto;
        pthread_cond_t idBalcaoGarcom;
        pthread_cond_t novaRodada;
        pthread_cond_t idBalcaoCliente;
        pthread_cond_t entregaProximo;

        int*** matrixGarcomPedidoCliente;
        int indexCliente;
        int indexBebida;

    protected:
        Bar();
        static Bar* inst;
};

#endif