#ifndef CLIENTE_H_
#define CLIENTE_H_

#include "bar.hpp"
#include <iostream>
#include <random>

class Cliente {
    public:
        Cliente(int);
        ~Cliente();
        int aleatorio(int,int);
        int fazPedido();
        void esperaPedido();
        void recebePedido();
        void consomePedido();
    private:
        int identificacao;
        int estadoCliente;
        int identificacaoGarcom;
        int bebidas;
        int rodadaCliente = 0;
};

#endif