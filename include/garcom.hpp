#ifndef GARCOM_H_
#define GARCOM_H_

#include "bar.hpp"

class Garcom {
    public:
        Garcom(int);
        ~Garcom();

        /*============== METODO RECEBER MAX PEDIDOS===============
            Realiza a coleta de informacoes dos pedidos
          ==================================================*/
        int recebeMaximoPedidos();

        /*============== METODO ENTREGA PEDIDOS===============
            Retorna o buffer individual de pedidos de cada garcom
        ao ser passado o ID do garcom.
          ==================================================*/
        void entregaPedidos();
    private:
        int bufferGarcons = 0;
        int identificacao;
        int estadoGarcom;
        int rodadaGarcom;
};

#endif