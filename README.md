# Controle e Simulação de Rodadas Grátis em um Bar

# Alunos

Bernardo Dalfovo de Souza - 18204849
Vinicius Slovinski - 18201356

# Como compilar:

Digite o make no terminal quando estiver na pasta do projeto

Para executar o programa, digite: ./exeBar *nC* *nG* *C* *nR*

Onde    *nC* = Numero de clientes
        *nG* = Numero de garçons
        *C*  = Capacidade
        *nR* = Numero de rodadas
